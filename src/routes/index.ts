import { Router } from 'express';
import clinicRoutes from './clinics';
export default (app: Router) => {
  clinicRoutes(app);
};
