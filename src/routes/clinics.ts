import { NextFunction, Request, Response, Router } from 'express';
import clinicsApi from './../constants/clinicsApi';
import ClinicController from '../controllers/clinic';
import StatusCodes from '../constants/statusCodes';
import SearchQuery from '../types/searchQuery';

export default (app: Router) => {
  app.get(
    clinicsApi.Clinics,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const { name, state, from, to }: SearchQuery = req.query;
        const { vet, dental } = await ClinicController.filter({
          name,
          state,
          from,
          to
        });
        return res.status(StatusCodes.OK).json({
          status: StatusCodes.OK,
          clinics: {
            vet,
            dental
          }
        });
      } catch (err) {
        return next(err);
      }
    }
  );
};
