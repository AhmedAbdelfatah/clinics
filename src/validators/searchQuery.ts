/**
 * ValidateSearchQuery
 * It's a class which validates query parameters (Name,State,From,To)
 * and throw errors as a fail first technique
 */
import ErrorMsg from '../constants/errorMessages';
import StatusCodes from '../constants/statusCodes';
import SearchQuery from '../types/searchQuery';
import ErrorException from '../utils/exception/error';
import stringToTimestamp from '../utils/stringToTimestamp';
import validateTimeFormat from '../utils/validateTime';

class ValidateSearchQuery {
  private readonly from?: string;
  private readonly to?: string;
  private readonly name?: string;
  private readonly state?: string;

  constructor({ from, to, name, state }: SearchQuery) {
    this.from = from;
    this.to = to;
    this.name = name;
    this.state = state;
  }
  private validateAvaliability() {
    if (this.from && !validateTimeFormat(this.from)) {
      throw new ErrorException(
        StatusCodes.BAD_REQUEST,
        ErrorMsg.TimeFromFormat
      );
    }
    if (this.to && !validateTimeFormat(this.to)) {
      throw new ErrorException(StatusCodes.BAD_REQUEST, ErrorMsg.TimeToFormat);
    }
    if (
      this.to &&
      this.from &&
      stringToTimestamp(this.to) < stringToTimestamp(this.from)
    ) {
      throw new ErrorException(
        StatusCodes.BAD_REQUEST,
        ErrorMsg.IncorrectTimeRange
      );
    }
  }
  private validateString(maxLength: number, key?: string, label?: string) {
    if (key) {
      const regex = /^[a-zA-Z0-9\s]+$/;
      if (!regex.test(key)) {
        throw new ErrorException(
          StatusCodes.BAD_REQUEST,
          ErrorMsg.IllegalInput
        );
      }
      const inputLength = key.trim().length;
      if (inputLength === 0) {
        throw new ErrorException(
          StatusCodes.BAD_REQUEST,
          `${label} ${ErrorMsg.NotEmpty}`
        );
      }
      if (inputLength > maxLength) {
        throw new ErrorException(
          StatusCodes.BAD_REQUEST,
          `${label} ${ErrorMsg.MaxLength} ${maxLength} characters`
        );
      }
    }
  }
  verify() {
    this.validateAvaliability();
    this.validateString(40, this.name, 'name');
    this.validateString(40, this.state, 'state');
  }
}
export default ValidateSearchQuery;
