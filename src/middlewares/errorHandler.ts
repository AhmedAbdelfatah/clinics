/**
 * errorHandler
 * It's a middleware to handle http exception on the application level (Transport layer)
 * by catching errors and throwing these errors
 */
import { Request, Response, NextFunction } from 'express';
import ErrorException from '../utils/exception/error';
import StatusCodes from '../constants/statusCodes';
import ErrorMsg from '../constants/errorMessages';
import logger from '../utils/logger';

const errorMiddleware = (
  error: ErrorException,
  _req: Request,
  res: Response,
  next: NextFunction
): Response | void => {
  try {
    const statusCode = error.statusCode || StatusCodes.INTERNAL_SERVER_ERROR;
    const msg = error.msg || ErrorMsg.ServerError;
    logger.error({ statusCode, msg });
    return res.status(statusCode).send({
      status: {
        code: statusCode,
        msg
      }
    });
  } catch (err) {
    return next(err);
  }
};
export default errorMiddleware;
