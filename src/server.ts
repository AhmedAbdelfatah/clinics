import express, { Application } from 'express';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import routes from './routes';
import errorMiddleware from './middlewares/errorHandler';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './swagger.json';

class App {
  public app: Application;
  constructor() {
    this.app = express();
    this.initializeConfig();
    this.registerRoutes();
    this.initializeErrorHandler();
  }

  private initializeConfig(): void {
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.app.use(compression());
    this.app.use(cors());
    this.app.use(helmet());
    this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  }
  private registerRoutes(): void {
    routes(this.app);
  }
  private initializeErrorHandler(): void {
    this.app.use(errorMiddleware);
  }
}
export default App;
