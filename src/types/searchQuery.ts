type SearchQuery = {
  name?: string;
  state?: string;
  from?: string;
  to?: string;
};
export default SearchQuery;
