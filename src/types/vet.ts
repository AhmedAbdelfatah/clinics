type VetClinic = {
  clinicName: string;
  stateCode: string;
  opening: {
    from: string;
    to: string;
  };
};
export default VetClinic;
