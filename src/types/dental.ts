type DentalClinic = {
  name: string;
  stateName: string;
  availability: {
    from: string;
    to: string;
  };
};
export default DentalClinic;
