import 'dotenv/config';
import App from './server';

const { app } = new App();
const PORT: number = Number(process.env.PORT) || 5001;

app.listen(PORT, () => {
  console.log(`🚀🚀🚀 Local-server: http://localhost:${PORT} 🚀🚀🚀`);
});
