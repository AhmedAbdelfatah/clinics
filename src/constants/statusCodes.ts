class StatusCodes {
  public static readonly OK: number = 200;
  public static readonly BAD_REQUEST: number = 400;
  public static readonly NOT_FOUND: number = 404;
  public static readonly METHOD_NOT_ALLOWED: number = 405;
  public static readonly PAYLOAD_TOO_LARGE: number = 413;
  public static readonly INTERNAL_SERVER_ERROR: number = 500;
}
export default StatusCodes;
