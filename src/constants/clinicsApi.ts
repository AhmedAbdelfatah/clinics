enum Api {
  Vet = 'https://storage.googleapis.com/scratchpay-code-challenge/vet-clinics.json',
  Dental = 'https://storage.googleapis.com/scratchpay-code-challenge/dental-clinics.json',
  Clinics = '/api/v1/clinics'
}
export default Api;
