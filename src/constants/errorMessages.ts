enum ErrorMsg {
  FetchFailed = 'Failed to fetch data',
  ServerError = 'Internal server Error',
  TimeFromFormat = 'Time from format no correct',
  TimeToFormat = 'Time to format no correct',
  IncorrectTimeRange = 'Time to should be greater than time from',
  IllegalInput = 'Input string contains illegal characters',
  MaxLength = 'Input length should not be greater than',
  NotEmpty = 'Input should not be empty'
}
export default ErrorMsg;
