import axios from 'axios';
import Clinic from '../../controllers/clinic';
import ClinicsApi from '../../constants/clinicsApi';
import ErrorMsg from '../../constants/errorMessages';
import ClinicController from '../../controllers/clinic';
import ValidateSearchQuery from '../../validators/searchQuery';
jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;
let mockGet: any;
const dentalClinics = [
  {
    name: 'Good Health Home',
    stateName: 'Alaska',
    availability: {
      from: '10:00',
      to: '19:30'
    }
  },
  {
    name: 'Mayo Clinic',
    stateName: 'Florida',
    availability: {
      from: '09:00',
      to: '20:00'
    }
  }
];
const vetClinics = [
  {
    clinicName: 'Good Health Home',
    stateCode: 'FL',
    opening: {
      from: '15:00',
      to: '20:00'
    }
  }
];
describe('ClinicController.get', () => {
  it('should return all clinics', async () => {
    mockedAxios.get.mockResolvedValueOnce({ data: dentalClinics });
    mockedAxios.get.mockResolvedValueOnce({ data: vetClinics });
    const clinics = await Clinic.get();
    expect(clinics).toEqual({ dental: dentalClinics, vet: vetClinics });
  });
  it('should handle errors', async () => {
    const error = new Error(ErrorMsg.FetchFailed);
    mockedAxios.get.mockRejectedValue(error);
    await expect(Clinic.get()).rejects.toThrowError(error);
    expect(mockedAxios.get).toHaveBeenCalledWith(ClinicsApi.Dental, {});
    expect(mockedAxios.get).toHaveBeenCalledWith(ClinicsApi.Vet, {});
  });
});
describe('ClinicController.filter()', () => {
  beforeEach(() => {
    mockGet = jest.fn().mockResolvedValue({
      dental: dentalClinics,
      vet: vetClinics
    });
    jest.spyOn(ClinicController, 'get').mockImplementation(mockGet);
  });
  it('should return an array of filtered clinics', async () => {
    const filterParams = { name: '', state: '', from: '', to: '' };
    const result = await ClinicController.filter(filterParams);
    expect(result).toEqual({ vet: vetClinics, dental: dentalClinics });
    expect(mockGet).toHaveBeenCalled();
  });
  it('should return one clinic based given state and time range', async () => {
    const filterParams = {
      name: '',
      state: 'Alaska',
      from: '10:01',
      to: '18:00'
    };
    const result = await ClinicController.filter(filterParams);
    expect(result).toEqual({
      dental: result.dental,
      vet: result.vet
    });
    expect(mockGet).toHaveBeenCalled();
  });
  it('should throw error if time from greater than time to', async () => {
    const filterParams = {
      name: '',
      state: '',
      from: '10:00',
      to: '09:00'
    };
    jest
      .spyOn(ValidateSearchQuery.prototype, 'verify')
      .mockImplementation(() => {
        throw new Error(ErrorMsg.IncorrectTimeRange);
      });
    expect(ClinicController.filter(filterParams)).rejects.toThrow(
      ErrorMsg.IncorrectTimeRange
    );
  });
  it('should throw error state/clinic name greater than 30 chars', async () => {
    const filterParams = {
      name: 'this is just for testing name are too long which is not correct we testing this now',
      state:
        'this is just for testing name are too long which is not correct we testing this now',
      from: '',
      to: ''
    };
    jest
      .spyOn(ValidateSearchQuery.prototype, 'verify')
      .mockImplementation(() => {
        throw new Error(ErrorMsg.MaxLength);
      });
    expect(ClinicController.filter(filterParams)).rejects.toThrow(
      ErrorMsg.MaxLength
    );
  });
  it('should throw an error if the name contains malicious code', async () => {
    const filterParams = {
      name: '<script>document.cookie();</script>',
      state: '',
      from: '',
      to: ''
    };
    jest
      .spyOn(ValidateSearchQuery.prototype, 'verify')
      .mockImplementation(() => {
        throw new Error('Invalid filter parameters');
      });

    try {
      await ClinicController.filter(filterParams);
    } catch (err) {
      expect(err).toEqual(new Error('Invalid filter parameters'));
    }
  });
  it('should throw an error with invalid time input type', async () => {
    const filterParams = {
      name: '  ping ',
      state: '    ',
      from: 'pong',
      to: 'ping'
    };
    jest
      .spyOn(ValidateSearchQuery.prototype, 'verify')
      .mockImplementation(() => {
        throw new Error('Invalid filter parameters');
      });

    try {
      await ClinicController.filter(filterParams);
    } catch (err) {
      expect(err).toEqual(new Error('Invalid filter parameters'));
    }
  });
});
