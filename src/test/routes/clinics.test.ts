import request from 'supertest';
import express, { Application } from 'express';
import api from '../../routes/clinics';
import Api from '../../constants/clinicsApi';
import ClinicController from '../../controllers/clinic';
import StatusCodes from '../../constants/statusCodes';
import ErrorException from '../../utils/exception/error';
import ErrorMsg from '../../constants/errorMessages';
let app: Application;
let server: any;

describe('GET /api/v1/clinics', () => {
  beforeEach(() => {
    app = express();
    server = app.listen(5009);
    api(app);
  });

  afterEach((done) => {
    server.close(done);
  });
  it('should return a status code of 200 and a list of clinics', async () => {
    const mockFilter = jest.fn().mockResolvedValue({
      vet: [
        {
          state: 'CA',
          opening: { from: '19:10', to: '00:00' },
          name: 'pink vet clinic'
        }
      ],
      dental: []
    });
    jest.spyOn(ClinicController, 'filter').mockImplementation(mockFilter);
    const res = await request(app).get(Api.Clinics);
    expect(res.status).toBe(StatusCodes.OK);
    expect(res.body).toEqual({
      status: StatusCodes.OK,
      clinics: {
        vet: [
          {
            state: 'CA',
            opening: { from: '19:10', to: '00:00' },
            name: 'pink vet clinic'
          }
        ],
        dental: []
      }
    });
  });
  it('should return a status code of 400 if clinic name has malicious code', async () => {
    const mockFilter = jest
      .fn()
      .mockRejectedValue(
        new ErrorException(StatusCodes.BAD_REQUEST, ErrorMsg.IllegalInput)
      );
    jest.spyOn(ClinicController, 'filter').mockImplementation(mockFilter);
    const res = await request(app).get(Api.Clinics + '?name=<script&nsn');
    expect(res.status).toBe(StatusCodes.BAD_REQUEST);
  });
});
