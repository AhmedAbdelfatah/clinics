import { VetClinic } from './../../types';
import { VetService } from './../../services/vet';

const vet: VetClinic[] = [
  {
    clinicName: 'National Veterinary Clinic',
    stateCode: 'CA',
    opening: { from: '9:00', to: '20:00' }
  },
  {
    clinicName: 'Animal First Clinic',
    stateCode: 'NY',
    opening: { from: '10:00', to: '18:00' }
  },
  {
    clinicName: 'Pets R Us Clinic',
    stateCode: 'CA',
    opening: { from: '9:00', to: '19:00' }
  }
];

describe('VetService', () => {
  let service: VetService;
  beforeEach(() => {
    service = new VetService(vet);
  });

  test('filterByState should filter the vet clinics by state', () => {
    service.filterByState('ca');
    expect(service.vet.length).toBe(2);
  });

  test('filterByName should filter the vet clinics by name', () => {
    service.filterByName('vete');
    expect(service.vet.length).toBe(1);
    expect(service.vet[0].clinicName).toBe('National Veterinary Clinic');
  });
  test('filterByAvailabilityFrom should filter the vet clinics by availability', () => {
    service.filterByAvailabilityFrom('9:30');
    expect(service.vet.length).toBe(2);
  });
  test('filterByAvailabilityTo should filter the vet clinics by availability', () => {
    service.filterByAvailabilityTo('19:00');
    expect(service.vet.length).toBe(2);
  });
  test('should trigger filter function and return the matched result in chain', () => {
    service.filter({
      state: 'ca',
      clinicName: 'Pets',
      AvailabilityFrom: '9:00',
      availabilityTo: '19:00'
    });
    expect(service.vet.length).toBe(1);
  });
});
