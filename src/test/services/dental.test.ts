import { DentalClinic } from './../../types';
import { DentalService } from './../../services/dental';

const dental: DentalClinic[] = [
  {
    name: 'Smile Makers Dental Clinic',
    stateName: 'CA',
    availability: { from: '9:00', to: '20:00' }
  },
  {
    name: 'Tooth Care Clinic',
    stateName: 'NY',
    availability: { from: '10:00', to: '18:00' }
  },
  {
    name: 'Perfect Smile Dental Clinic',
    stateName: 'CA',
    availability: { from: '9:00', to: '19:00' }
  }
];

describe('DentalService', () => {
  let service: DentalService;
  beforeEach(() => {
    service = new DentalService(dental);
  });

  test('filterByState should filter the dental clinics by state', () => {
    service.filterByState('ca');
    expect(service.dental.length).toBe(2);
  });

  test('filterByName should filter the dental clinics by name', () => {
    service.filterByName('smile');
    expect(service.dental.length).toBe(2);
    expect(service.dental[0].name).toBe('Smile Makers Dental Clinic');
  });
  test('filterByAvailabilityFrom should filter the dental clinics by availability', () => {
    service.filterByAvailabilityFrom('9:30');
    expect(service.dental.length).toBe(2);
  });
  test('filterByAvailabilityTo should filter the dental clinics by availability', () => {
    service.filterByAvailabilityTo('19:00');
    expect(service.dental.length).toBe(2);
  });
  test('should trigger filter function and return the matched result in chain', () => {
    service.filter({
      state: 'ca',
      clinicName: 'Makers',
      AvailabilityFrom: '9:00',
      availabilityTo: '19:00'
    });
    expect(service.dental.length).toBe(1);
  });
});
