/**
 * VetService
 * Filter interface for vet service to filter
 *  by chining the clinics array and return the final result
 * filterByState
 * filterByStateName
 * filterByAvailabilityFrom
 * filterByAvailabilityTo
 * finally the chain should return the result
 */
import { VetClinic } from '../types';
import stringToTimestamp from '../utils/stringToTimestamp';

export class VetService {
  public vet: VetClinic[];
  constructor(vet: VetClinic[]) {
    this.vet = vet;
  }
  filterByState(state?: string) {
    if (state) {
      const regex = new RegExp(state, 'i');
      this.vet = this.vet.filter(({ stateCode }) => {
        return regex.test(stateCode);
      });
    }
    return this;
  }
  filterByName(name?: string) {
    if (name) {
      const regex = new RegExp(name, 'i');
      this.vet = this.vet.filter(({ clinicName }) => {
        return regex.test(clinicName);
      });
    }
    return this;
  }
  filterByAvailabilityFrom(AvailabilityFrom?: string) {
    if (AvailabilityFrom) {
      this.vet = this.vet.filter(({ opening }) => {
        return (
          stringToTimestamp(AvailabilityFrom) >=
            stringToTimestamp(opening.from) &&
          stringToTimestamp(AvailabilityFrom) <= stringToTimestamp(opening.to)
        );
      });
    }
    return this;
  }
  filterByAvailabilityTo(availabilityTo?: string) {
    if (availabilityTo) {
      this.vet = this.vet.filter(({ opening }) => {
        return (
          stringToTimestamp(availabilityTo) <= stringToTimestamp(opening.to) &&
          stringToTimestamp(availabilityTo) >= stringToTimestamp(opening.from)
        );
      });
    }
    return this;
  }
  filter({
    state,
    clinicName,
    AvailabilityFrom,
    availabilityTo
  }: {
    state?: string;
    clinicName?: string;
    AvailabilityFrom?: string;
    availabilityTo?: string;
  }) {
    return this.filterByName(clinicName)
      .filterByState(state)
      .filterByAvailabilityFrom(AvailabilityFrom)
      .filterByAvailabilityTo(availabilityTo);
  }
}
