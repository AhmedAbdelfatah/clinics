/**
 * DentalService
 * Filter interface for dental service to filter
 *  by chining the clinics array and return the final result
 * filterByState
 * filterByStateName
 * filterByAvailabilityFrom
 * filterByAvailabilityTo
 * finally the chain should return the result
 */
import { DentalClinic } from '../types';
import stringToTimestamp from '../utils/stringToTimestamp';

export class DentalService {
  public dental: DentalClinic[];
  constructor(dental: DentalClinic[]) {
    this.dental = dental;
  }
  filterByState(state?: string) {
    if (state) {
      const regex = new RegExp(state, 'i');
      this.dental = this.dental.filter(({ stateName }) => {
        return regex.test(stateName);
      });
    }
    return this;
  }
  filterByName(clinicName?: string) {
    if (clinicName) {
      const regex = new RegExp(clinicName, 'i');
      this.dental = this.dental.filter(({ name }) => {
        return regex.test(name);
      });
    }
    return this;
  }
  filterByAvailabilityFrom(AvailabilityFrom?: string) {
    if (AvailabilityFrom) {
      this.dental = this.dental.filter(({ availability: { from, to } }) => {
        return (
          stringToTimestamp(AvailabilityFrom) >= stringToTimestamp(from) &&
          stringToTimestamp(AvailabilityFrom) <= stringToTimestamp(to)
        );
      });
    }
    return this;
  }
  filterByAvailabilityTo(availabilityTo?: string) {
    if (availabilityTo) {
      this.dental = this.dental.filter(({ availability: { to, from } }) => {
        return (
          stringToTimestamp(availabilityTo) <= stringToTimestamp(to) &&
          stringToTimestamp(availabilityTo) >= stringToTimestamp(from)
        );
      });
    }
    return this;
  }
  filter({
    state,
    clinicName,
    AvailabilityFrom,
    availabilityTo
  }: {
    state?: string;
    clinicName?: string;
    AvailabilityFrom?: string;
    availabilityTo?: string;
  }) {
    return this.filterByName(clinicName)
      .filterByState(state)
      .filterByAvailabilityFrom(AvailabilityFrom)
      .filterByAvailabilityTo(availabilityTo);
  }
}
