/**
 * ClinicsController
 * Fetch the data from the remote API
 * validate search query then call filter dental/vet serivce
 * returning the result
 */
import ClinicsApi from '../constants/clinicsApi';
import ErrorMsg from '../constants/errorMessages';
import { VetClinic, DentalClinic } from './../types';
import fetch from '../utils/fetch';
import { VetService } from './../services/vet';
import { DentalService } from './../services/dental';
import ValidateSearchQuery from '../validators/searchQuery';
import SearchQuery from '../types/searchQuery';
import ErrorException from '../utils/exception/error';
import StatusCodes from '../constants/statusCodes';

class ClinicController {
  static async get(): Promise<
    { dental: [DentalClinic]; vet: [VetClinic] } | never
  > {
    try {
      const dentalRequest = fetch(ClinicsApi.Dental);
      const vetRequest = fetch(ClinicsApi.Vet);
      const [dentalClinics, vetClinics] = await Promise.all([
        dentalRequest,
        vetRequest
      ]);
      return {
        dental: dentalClinics.data,
        vet: vetClinics.data
      };
    } catch (err) {
      throw new ErrorException(StatusCodes.BAD_REQUEST, ErrorMsg.FetchFailed);
    }
  }
  static async filter({ name, state, from, to }: SearchQuery) {
    const { vet, dental } = await ClinicController.get();
    if (name || state || from || to) {
      const validateSearchQuery = new ValidateSearchQuery({
        name,
        state,
        from,
        to
      });
      validateSearchQuery.verify();
      const vetService = new VetService(vet);
      const dentalService = new DentalService(dental);
      const vetResult = vetService.filter({
        clinicName: name,
        availabilityTo: to,
        AvailabilityFrom: from,
        state: state
      });
      const dentalResult = dentalService.filter({
        clinicName: name,
        availabilityTo: to,
        AvailabilityFrom: from,
        state: state
      });
      return { vet: vetResult.vet, dental: dentalResult.dental };
    } else {
      return { vet, dental };
    }
  }
}
export default ClinicController;
