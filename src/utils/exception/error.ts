class ErrorException extends Error {
  public statusCode: number;
  public msg: string;
  constructor(statusCode: number, msg: string) {
    super(msg);
    this.statusCode = statusCode;
    this.msg = msg;
  }
}

export default ErrorException;
