const validateTime = (time: string) => {
  const regex = /^(([01]?\d|2[0-4])(:([0-5]\d))?|24:00)$/;
  return regex.test(time);
};
export default validateTime;
