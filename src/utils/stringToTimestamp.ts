import moment from 'moment';
export default (timeString: string): number => {
  return moment(timeString, 'HH:mm').valueOf();
};
