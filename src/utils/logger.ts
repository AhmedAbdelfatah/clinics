import path from 'path';
import { createLogger, transports } from 'winston';

const options = {
  file: {
    level: 'error',
    filename: path.join(__dirname, '/../logs/error.log'),
    handleExceptions: true,
    json: true,
    maxsize: 5242880,
    maxFiles: 5,
    colorize: false
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true
  }
};

const logger = createLogger({
  transports: [
    new transports.File(options.file),
    new transports.Console(options.console)
  ],
  exitOnError: false
});

export default logger;
