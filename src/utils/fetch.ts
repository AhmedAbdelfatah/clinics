import axios from 'axios';

const fetch = (url: string, options = {}) => {
  return axios.get(url, options);
};
export default fetch;
