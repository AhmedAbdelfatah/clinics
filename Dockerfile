FROM node:18-alpine
WORKDIR /scratchpay 
COPY package*.json ./
RUN yarn install 
COPY . . 
RUN yarn build 
EXPOSE $PORT
CMD ["yarn", "start", "--port", "$PORT"]