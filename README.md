## :ledger: Clinics search API

Clinics search API it's one end point **`/api/v1/clinics`** which returns the clinics as a search result by calling these two APIs

- :pushpin: [**Vet clinics**](https://storage.googleapis.com/scratchpay-code-challenge/vet-clinics.json)
- :pushpin: [**Dental clinics**](https://storage.googleapis.com/scratchpay-code-challenge/dental-clinics.json)

## Important links

- :rocket: :rocket: [Deployment link](https://scratch-pay.onrender.com/docs/) :point_left: :point_left:
- :rocket: :rocket: [Docker image](registry.gitlab.com/ahmedabdelfatah/clinics/main) :point_left: :point_left:

## Tech-Stack

- :office: Scaffolding ( Husky , prettier , linter)
- :dart: CI/CD (Gitlab pipline with the following stages)
  - :one: Install dependancies.
  - :two: Building the code.
  - :three: Testing
    - code style test
    - code base test (unit-test & integration test using supertest)
  - :four: Publishing stage to build a docker image in container registery
- :steam_locomotive: Backend (NodeJS Typescript)
- :steam_locomotive: API (REST)
- :telephone: Git (trunk based development)
- Testing (Jest,supertest)
- Logs (Winston)
- :rocket: Deployment (Docker,CI/CD)
- :rocket: Documentation (Swagger open API)
  - Local-server: http://localhost:5005/docs
  - Remote-server: https://scratch-pay.onrender.com/docs/

## :eyes: Getting Started

Clone the project , install the dependancies and run it locally as development or you can pull the docker image from the container registery and run it locally.

### :bangbang: Prerequisites

Make sure that your environment setup has the follwoing installed on your machine:

- NodeJS v18.13.0
- Typescript
- Docker If you want run the image locally
- Git

### :key: Environment Variables

To run this project, you will need to add the following environment variables to your `.env` file and `.env.example` already pushed to gitlab as a reference

`PORT`

<!-- Installation -->

### :gear: Running locally from dev environment

- Clone the repo by running the following command
  ```
  git clone https://gitlab.com/AhmedAbdelfatah/clinics.git
  cd clinics
  ```
- Install the dependancies by running the following command

  ```
  yarn
  ```

- Running the project
  ```
  yarn run start:dev
  ```

:triangular_flag_on_post: To use and try API from Swagger UI https://localhost:5005/docs

You can also use the following command to run the test `yarn run test`

### :gear: Running the project by using docker

Make sure first the docker installed on your machine then run the following command to pull the remote image.

```
docker pull registry.gitlab.com/ahmedabdelfatah/clinics/main
```

To run this image run the following command

```
docker run -p5005:5005 -e PORT=5005 registry.gitlab.com/ahmedabdelfatah/clinics/main:latest
```

:triangular_flag_on_post: To use and try API from Swagger UI https://localhost:5005/docs

## Swagger UI

- name: a string (Max length is 40 chars) ex: clinic name `national vet clinic` valid search cases will be `vet` | `cli` `national` and so on ..
- state can: a string (Max length is 40 chars)
- to: time format can be represented in h,hh,hh:mm ex: 9 or 09 or 09:00
- from: time format can be represented in h,hh,hh:mm 9 or 09 or 09:00

## Assumption

- I assumed that vet & clinic different data model (Two entities) that's why i created two services for them to handle the filter method and retrun the dental and vet array in the response to avoid creating a dapter and looping through data which is o(n) if we mapped one model to the other one. Also in the future if there are some extra features , fields or methods we can add it separately. **I know that we can define one filter interface to avoid duplication and this could be a valid point but i assumed this to handle vet and clinic as a separate entity**

- As it's mentioned in the requirement do not use full text search or database so the way that can helps to deal with large data:
  - Pagination but it requires multiple request calls for filter till reaching to the last element in json reponse.
  - Using memoryDB like redis which enables to cache large amount of data.
  - Using DB and we can index state,clinic name to make the search process faster with complexity o(1) to avoid table scanning.
  - Using Readable streams which enable us to read data and process it in chunks without saving it on memory which is memory optimized.

Any solution of these can handle the large data set and the memory issue.
